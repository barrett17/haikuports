SUMMARY="A high-performance asynchronous HTTP client library"
DESCRIPTION="
The serf library is a C-based HTTP client library built upon the Apache \
Portable Runtime (APR) library. It multiplexes connections, running the \
read/write communication asynchronously. Memory copies and transformations are \
kept to a minimum to provide high performance operation.
"
HOMEPAGE="http://code.google.com/p/serf/"
LICENSE="Apache v2"
COPYRIGHT="2002-2013 Justin Erenkrantz and Greg Stein"
SRC_URI="http://serf.googlecode.com/svn/src_releases/serf-$portVersion.tar.bz2"
CHECKSUM_SHA256="ecccb74e665e6ea7539271e126a21d0f7eeddfeaa8ce090adb3aec6682f9f0ae"
REVISION="1"
ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"
PATCHES="serf-1.3.7.patchset"

PROVIDES="
	serf$secondaryArchSuffix = $portVersion
	lib:libserf_1$secondaryArchSuffix = 1.3.0 compat >= 1
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libssl$secondaryArchSuffix
	lib:libcrypto$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	lib:libapr_1$secondaryArchSuffix
	lib:libaprutil_1$secondaryArchSuffix
	# aprutil brings us -lexpat and -liconv
	lib:libexpat$secondaryArchSuffix
	lib:libiconv$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libssl$secondaryArchSuffix
	devel:libcrypto$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	devel:libapr_1$secondaryArchSuffix
	devel:libaprutil_1$secondaryArchSuffix
	# aprutil brings us -lexpat and -liconv
	devel:libexpat$secondaryArchSuffix
	devel:libiconv$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	cmd:gcc$secondaryArchSuffix
	#cmd:make
	cmd:scons >= 2.3
	"
BUILD()
{
	scons $jobArgs OPENSSL=$portPackageLinksDir/devel~libssl$secondaryArchSuffix \
		ZLIB=/system APR=/bin/apr-1-config APU=/bin/apu-1-config PREFIX=$prefix
}

INSTALL()
{
	scons install OPENSSL=$portPackageLinksDir/devel~libssl$secondaryArchSuffix \
		ZLIB=/system APR=/bin/apr-1-config APU=/bin/apu-1-config PREFIX=$prefix
	
	mkdir -p $includeDir $developLibDir
	mv $prefix/include/serf-1/* $includeDir/
	rm -rf $prefix/include
	
	# prepare development lib links
	prepareInstalledDevelLib libserf-1
	
	fixPkgconfig
	
	# devel package
	packageEntries devel $developDir
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	serf${secondaryArchSuffix}_devel = $portVersion
	devel:libserf_1$secondaryArchSuffix = 1.3.0 compat >= 1
	"
REQUIRES_devel="
	serf$secondaryArchSuffix == $portVersion base
	"
